<?php
date_default_timezone_set('America/Guayaquil');
/**
 * @filesource  /home/payrabbit/www/page/manage/payment.php
 * @Descripcion pagina web - informacion del Cliente
 *
 *
 * PHP version 5
 *
 * @autor       Alejandro Barrionuevo R. <mbarrionuevo@anyway.com.ec>
 * @includes    1. /home/payrabbit/www/lib/Common.php
 *              2.
 * @fecha       2019/07/03
 * @flujo
 *
 * @Control de Cambio
 * @autor
 * @fecha
 * Descripcion
 */
require_once '/home/payrabbit/www/lib/Common.php';
$ipServer      = $_SERVER['SERVER_ADDR'];
$token         = isset($_REQUEST['token']) ? $_REQUEST['token'] : '';
$env           = $ipServer == Common::IP_DEV ? 'dev' : 'prod';
$amount        = 0;
$iva           = 0;
$ivaReturn     = 0;
$idTransaction = 0;
$msgError      = 'Token inválido';
if (strlen($token)) {
    $beginTime = microtime(true);
    $data      = [];
    $headers = array(
        'Content-Type: application/json'
    );
    $url = str_replace("{token}", $token, $envParams[$env]['url_keos_gettransaction']);
    $getTransaction = Common::coreRequest($data, $url, Common::GET_METHOD, $headers, Common::JSON_METHOD);
    $rTransaction   = json_decode($getTransaction, true);
    if ($rTransaction["code"]==200){
        $amount        = $rTransaction['data']['amount_due'];
        $iva           = $rTransaction['data']['tax'];
        $ivaReturn     = 0;
        $idTransaction = $rTransaction['data']['tn'];
    }else{
        $msgError = $rTransaction["message"];
    }

    $endTime   = microtime(true);
    $totalTime = $endTime - $beginTime;
    $method    = 'getTransaction';
    Common::registrarLog($method, json_encode($data), $getTransaction, $totalTime, $envParams[$env]['path_payment_log']);
}
?>
<html>
    <head>
        <title><?php echo Common::GESTION_INFO_CLIENTE_TITLE ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>fontawesome/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>select2/4.0.7/css/select2.min.css" />
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>jquery-loading/1.3.0/jquery.loading.min.css" />
        <link rel="stylesheet" href="/css/payment.css?<?php echo date("YmdHis") ?>" >
    </head>
    <body>
        <div class="row no-gutters shadow-sm mb-4 bg-white page-heading">
            <div class="col-12">
                <h2 class="text-center"><?php echo Common::GESTION_INFO_CLIENTE_TITLE ?></h2>
            </div>
        </div>
<?php
        if ($amount > 0) {
?>
        <div class="row no-margin bg-white">
            <div class="col-10 col-sm-8 col-md-6 col-lg-4 offset-1 offset-sm-2 offset-md-3 offset-lg-4">
                <div class="row border-bottom">
                    <div class="col-6">Total a pagar</div>
                    <div class="col-6 text-center">$<?php echo number_format($amount, 2, '.', ',') ?></div>
                </div>
                <form class="mt-3">
                    <div class="row no-margin">
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-5 col-form-label"><?php echo Common::NOMBRE_TARJETAHABIENTE_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="nombre" maxlength="<?php echo Common::PAGO_FORM_NOMBRE_MAXLENGHT ?>" placeholder="<?php echo Common::PAGO_FORM_NOMBRE_PLACEHOLDER ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="apellido" maxlength="<?php echo Common::PAGO_FORM_APELLIDO_MAXLENGHT ?>" placeholder="<?php echo Common::PAGO_FORM_APELLIDO_PLACEHOLDER ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="form-group row">
                                <label for="genero" class="col-5 col-form-label"><?php echo Common::GENERO_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <select class="form-control" id="genero"></select>
                                </div>
                            </div>-->
                            <div class="form-group row">
                                <label for="departamento" class="col-5 col-form-label"><?php echo Common::DEPARTAMENTO_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <select class="form-control" id="departamento"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ciudad" class="col-5 col-form-label"><?php echo Common::CIUDAD_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <select class="form-control" id="ciudad"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="direccion" class="col-5 col-form-label"><?php echo Common::DIRECCION_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <input type="text" class="form-control" id="direccion" maxlength="<?php echo Common::PAGO_FORM_DIRECCION_MAXLENGHT ?>" placeholder="<?php echo Common::PAGO_FORM_DIRECCION_PLACEHOLDER ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="postal" class="col-5 col-form-label"><?php echo Common::CODIGO_POSTAL_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <input type="text" class="form-control" id="postal" maxlength="<?php echo Common::PAGO_FORM_CODIGO_POSTAL_MAXLENGHT ?>" placeholder="<?php echo Common::PAGO_FORM_CODIGO_POSTAL_PLACEHOLDER ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telefono" class="col-5 col-form-label"><?php echo Common::TELEFONO_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <input type="tel" class="form-control" id="telefono" maxlength="<?php echo Common::PAGO_FORM_TELEFONO_MAXLENGHT ?>" placeholder="<?php echo Common::PAGO_FORM_TELEFONO_PLACEHOLDER ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-5 col-form-label"><?php echo Common::EMAIL_TEXT . ' ' . Common::PAGO_FORM_INPUT_REQUERIDO ?></label>
                                <div class="col-7">
                                    <input type="email" class="form-control" id="email" maxlength="<?php echo Common::PAGO_FORM_EMAIL_MAXLENGHT ?>" placeholder="<?php echo Common::PAGO_FORM_EMAIL_PLACEHOLDER ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class='card-wrapper'></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="border-0 form-control">
                                    <p class="text-dark small"><?php echo Common::PAGO_FORM_INFO_TEXT ?></p>
                                </div>
                            </div>
                            <div class="d-flex flex-row-reverse">
                                <button type="button" class="btn btn-info" id="continuar"><?php echo Common::CONTINUAR_TEXT ?></button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="idacquirer" value="<?php echo $envParams[$env]['vpos_idacquirer'] ?>" />
                    <input type="hidden" id="idcommerce" value="<?php echo $envParams[$env]['vpos_idcommerce'] ?>" />
                    <input type="hidden" id="terminalcode" value="<?php echo $envParams[$env]['vpos_terminalcode'] ?>" />
                    <input type="hidden" id="amount" value="<?php echo $amount ?>" />
                    <input type="hidden" id="iva" value="<?php echo $iva ?>" />
                    <input type="hidden" id="ivareturn" value="<?php echo $ivaReturn ?>" />
                    <input type="hidden" id="idtrans" value="<?php echo $idTransaction ?>" />
                </form>
            </div>
        </div>
<?php
        } else {
?>
        <div class="row no-margin bg-white">
            <div class="col-10 col-sm-8 col-md-6 offset-1 offset-sm-2 offset-md-3">
                <div class="form-group row">
                    <div class="border-0 form-control">
                        <p class="text-danger text-center"><?php echo $msgError ?></p>
                    </div>
                </div>
            </div>
        </div>
<?php
        }
?>
    </body>
    <!-- jquery -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>popper.js/1.14.7/umd/popper.min.js" ></script>
    <!-- bootstrap -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- sweetalert -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>sweetalert2/8.11.4/sweetalert2.all.min.js"></script>
    <!-- select2 -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>select2/4.0.7/js/select2.full.min.js"></script>
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>select2/4.0.7/i18n/es.js"></script>
    <!-- jquery-loading -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>jquery-loading/1.3.0/jquery.loading.min.js"></script>
    <!-- functional scripts -->
    <script src="/js/Common.js?<?php echo date("YmdHis") ?>"></script>
    <script src="/js/payment2.js?<?php echo date("YmdHis") ?>"></script>
</html>