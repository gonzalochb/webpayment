<?php

    /********** INCLUDE DE LIBRERIAS ***************/
    include '/lib/mySoap/ConsultaTx.php';
    include '/beans/Includes.php';      
    /***********************************************/
    
    /********** DEFINICIÓN DE WSDL *****************/
    $wsdl = 'http://172.19.202.5:9080/vpos2/services/VPOS2RESULTTXSOAP/META-INF/VPOS2RESULTTXSOAP.wsdl';
    /***********************************************/
        
    /********** REFERECIAS DE CERTIFICADOS *********/
    /*SEND*/
    define('CRYPTO_PUBLIC_SEND', './certificates/ALIGNET.TESTING.NOPHP.CRYPTO.PUBLIC.txt');     
    define('FIRMA_PRIVATE_SEND', './certificates/llaveprivadafirmaPSP.txt'); 
    /*RECIVE*/
    define('SIGNATURE_PUBLIC_RECIVE', './certificates/ALIGNET.TESTING.NOPHP.SIGNATURE.PUBLIC.txt'); 
    define('CIFRADO_PRIVATE_RECIVE', './certificates/llaveprivadacifradoPSP.txt'); 
    /***********************************************/
                      
    $consultaTx = new ConsultaTx();       
    $vposConsulta = $consultaTx->consultaEstadoTx("138", "1024", "069467021", CRYPTO_PUBLIC_SEND, FIRMA_PRIVATE_SEND, SIGNATURE_PUBLIC_RECIVE, CIFRADO_PRIVATE_RECIVE, "2cb3a5f8b93dbb42", $wsdl);
    print_r($vposConsulta);
    
?>

