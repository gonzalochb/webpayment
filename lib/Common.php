<?php

/**
 * @filesource  /home/payrabbit/www/lib/Common.php
 * @Descripcion Constantes y funciones comunes para el proyecto
 *
 *
 * PHP version 5
 *
 * @autor       Alejandro Barrionuevo R. <mbarrionuevo@anyway.com.ec>
 * @includes    1. 
 *              2. 
 * @fecha       2019/07/03
 * @flujo       
 * 
 * @Control de Cambio
 * @autor      
 * @fecha      
 * Descripcion 
 */
$envParams = array(
    'dev' => array(
        'vpos_idacquirer' => '1', 'vpos_idcommerce' => '1572',
        'vpos_terminalcode' => '00029802', 'vpos_codigounico' => '014248256',
        'vpos_url' => 'https://testecommerce.credibanco.com/vpos2/MM/transactionStart20.do',
        'path_public_firma_key' => '/home/payrabbitdev/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.firma.publica.txt',
        'path_public_cifrado_key' => '/home/payrabbitdev/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.cifrado.publica.txt',
        'path_private_firma_key' => '/home/payrabbitdev/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.firma.privada.txt',
        'path_private_cifrado_key' => '/home/payrabbitdev/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.cifrado.privada.txt',
        'path_vpos_crb_crypto_key' => '/home/payrabbitdev/www/lib/vpos-plugin-php-2.0.3/certificates/LLAVE.VPOS.CRB.CRYPTO.1024.X509.txt',
        'path_vpos_crb_sign_key' => '/home/payrabbitdev/www/lib/vpos-plugin-php-2.0.3/certificates/LLAVE.VPOS.CRB.SIGN.1024.X509.txt',
        'vector_inicializacion' => '51bb0b0a0e5177ae',
        //'url_keos_gettransaction'  => 'http://devapi.clipclap.co/parse/functions/getTransaction',
        'url_keos_gettransaction' => 'https://process-gw-uat.payrabbit.co/psp/card/checkout-web/order/{token}',
        'url_keos_updateState' => 'http://devapi.clipclap.co/parse/functions/setTransactionState',
        'url_vpos_response' => 'https://localhost/payrabbitdev/notification/vpos_response.php',
        'path_payment_log' => '/home/payrabbitdev/log/payment.log',
        'keos_application_id' => 'PYIdN91azqtxVMsUurkSlR5URTsRohffKvK5pedx',
        'keos_javascript_key' => 'hbwejFzSKmP4ALaol6QJu3tVMUau9BPgkIhSASBw',
        'keos_client_version' => 'js1.11.1',
        'ipay_api_usr' => 'CLIP_CLAP-api',
        'ipay_api_pwd' => 'Colombia25*',
        'ipay_api_register' => 'https://ecouat.credibanco.com/payment/rest/register.do',
        'ipay_api_order_status' => 'https://ecouat.credibanco.com/payment/rest/getOrderStatusExtended.do',
        'core_set_psp_order' => 'https://mybdes.payrabbit.co/payments/paylink/credibanco/ipay-order-register/{crypt_token}',
        'core_set_psp_confirm' => 'https://mybdes.payrabbit.co/payments/paylink/credibanco/ipay-confirm/{ticket_number}',
        'callback_url' => 'https://webpaymentdev.payrabbit.co/notification/ipay_callback.php',
    ),
    'prod' => array(
        'vpos_idacquirer' => '1', 'vpos_idcommerce' => '6951',
        'vpos_terminalcode' => '00003398', 'vpos_codigounico' => '014248256',
        'vpos_url' => 'https://ecommerce.credibanco.com/vpos2/MM/transactionStart20.do',
        'path_public_firma_key' => '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.firma.publica.txt',
        'path_public_cifrado_key' => '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.cifrado.publica.txt',
        'path_private_firma_key' => '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.firma.privada.txt',
        'path_private_cifrado_key' => '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/certificates/payrabbit.cifrado.privada.txt',
        'path_vpos_crb_crypto_key' => '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/certificates/LLAVE.VPOS.CRB.CRYPTO.1024.X509.txt',
        'path_vpos_crb_sign_key' => '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/certificates/LLAVE.VPOS.CRB.SIGN.1024.X509.txt',
        'vector_inicializacion' => '8815cfbb68b77eec',
        //'url_keos_gettransaction'  => 'https://myb.payrabbit.co/payments/paylink/get-payment/{token}', #'https://api.clipclap.co/parse/functions/getTransaction',
        'url_keos_gettransaction' => 'https://process-gw.payrabbit.co/psp/card/checkout-web/order/{token}',
        'url_keos_updateState' => 'https://myb.payrabbit.co/payments/paylink/credibanco/notification/{token}', #'https://api.clipclap.co/parse/functions/setTransactionState',
        'url_keos_log' => 'https://myb.payrabbit.co/payments/logdb/psp-credibanco',
        'url_vpos_response' => 'https://localhost/payrabbit/notification/vpos_response.php',
        'path_payment_log' => '/home/payrabbit/log/payment.log',
        'keos_application_id' => 'V8De2XoYO6exPxib6nHaB8Yli5YtyfB4p84E1wRQ',
        'keos_javascript_key' => 'huiDEFcOVp9XAN84QyNOR17Jzik9AAPw5dLmY0eX',
        'keos_client_version' => 'js1.11.1',
        'ipay_api_usr' => '663IPAY_24-api',
        'ipay_api_pwd' => 'k24sP1yr1bb3t+',
        'ipay_api_register' => 'https://eco.credibanco.com/payment/rest/register.do',
        'ipay_api_order_status' => 'https://eco.credibanco.com/payment/rest/getOrderStatusExtended.do',
        'core_set_psp_order' => 'https://myb.payrabbit.co/payments/paylink/credibanco/ipay-order-register/{crypt_token}',
        'core_set_psp_confirm' => 'https://myb.payrabbit.co/payments/paylink/credibanco/ipay-confirm/{ticket_number}',
        'callback_url' => 'https://webpayment.payrabbit.co/notification/ipay_callback.php',
    )
);

class Common
{

    const IP_DEV = '10.142.0.3';
    const IP_PROD = '35.247.108.102';
    const ERROR_CODE_SUCCESSFUL = '00';
    const ESTADO_TRANSACCION_EXITOSA = 1;
    const ESTADO_TRANSACCION_FALLIDA = 0;
    const PATH_PROYECTO = '/';
    const PATH_RECURSOS_GENERALES = '/src/ajax/libs/';
    const POST_METHOD = 'POST';
    const JSON_METHOD = 'json';
    const GET_METHOD = 'GET';
    const GESTION_INFO_PAGOS_TITLE = 'Información del Pago';
    const GESTION_INFO_CLIENTE_TITLE = 'Información del Cliente';
    const GESTION_COMPROBANTE_TITLE = 'Voucher Electrónico';
    const DOCUMENTO_TEXT = 'Documento';
    const NUMERO_DOCUMENTO_TEXT = 'Número de Documento';
    const NUMERO_TARJETA_TEXT = 'Número de Tarjeta';
    const CODIGO_SEGURIDAD_TEXT = 'Código de Seguridad';
    const FECHA_VENCIMIENTO_TEXT = 'Fecha de Vencimiento';
    const TIPO_DOCUMENTO_TEXT = 'Tipo de Documento';
    const DEPARTAMENTO_TEXT = 'Departamento';
    const GENERO_TEXT = 'Género';
    const CIUDAD_TEXT = 'Ciudad';
    const DIRECCION_TEXT = 'Dirección';
    const CODIGO_POSTAL_TEXT = 'Código Postal';
    const TELEFONO_TEXT = 'Teléfono';
    const EMAIL_TEXT = 'Email';
    const NOMBRE_TEXT = 'Nombre';
    const NOMBRE_COMPLETO_TEXT = 'Nombre Completo';
    const NOMBRE_TARJETAHABIENTE_TEXT = 'Tarjetahabiente';
    const PAGAR_TEXT = 'Pagar';
    const CONTINUAR_TEXT = 'Continuar';
    const VENCIMIENTO_SEGURIDAD_TEXT = 'Fecha de Vencimiento / Código de Seguridad';
    const CODIGO_MONEDA_COLOMBIA = '170';
    const CODIGO_MONEDA_DOLARES = '840';
    const MONEDA_COLOMBIA = 'COP';
    const MONEDA_DOLARES = 'USD';
    const CODIGO_PAIS_ISO_COLOMBIA = 'CO';
    const CODIGO_LENGUAJE = 'SP';
    const VALOR_SIN_IVA = 0;
    const VPOS_PLANID = '01';
    const VPOS_QUOTAID = '001'; // numero de cuotas 001, 003, 012, etc
    const CODIGO_NACIONALIDAD = 'CO';
    const OBSERVACION_GENERICA = 'Pago clip clap';
    const IMPRIMIR_TEXT = 'Imprimir';
    const MENSAJE_CONFORMIDAD = 'PAGARÉ INCONDICIONALMENTE Y A LA ORDEN DEL ACREEDOR EL VALOR TOTAL DE ESTE PAGARÉ JUNTO CON LOS INTERESES A LAS TASAS MÁXIMAS PERMITIDAS POR LA LEY';
    /*     * ******************** FORMULARIO DE PAGO *************************** */
    const PAGO_FORM_INPUT_REQUERIDO = '<span>*</span>';
    const PAGO_FORM_INFO_TEXT = '* CAMPOS REQUERIDOS';
    const PAGO_FORM_DIRECCION_MAXLENGHT = 50;
    const PAGO_FORM_TELEFONO_MAXLENGHT = 15;
    const PAGO_FORM_EMAIL_MAXLENGHT = 50;
    const PAGO_FORM_CODIGO_POSTAL_MAXLENGHT = 9;
    const PAGO_FORM_DOCUMENTO_PLACEHOLDER = 'Documento';
    const PAGO_FORM_DIRECCION_PLACEHOLDER = 'Ej: Cra 5 # 10-02';
    const PAGO_FORM_TELEFONO_PLACEHOLDER = 'Ej: 3215244879';
    const PAGO_FORM_EMAIL_PLACEHOLDER = 'Ej: nombre@dominio.com';
    const PAGO_FORM_CODIGO_POSTAL_PLACEHOLDER = 'Ej: 194576';
    const PAGO_FORM_NOMBRE_PLACEHOLDER = 'Nombre';
    const PAGO_FORM_APELLIDO_PLACEHOLDER = 'Apellido';
    const PAGO_FORM_NOMBRE_MAXLENGHT = 16;
    const PAGO_FORM_APELLIDO_MAXLENGHT = 16;

    public static function coreRequest($data, $url, $type = 'POST', $headers = array(), $method = 'json')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($type == Common::POST_METHOD) {
            curl_setopt($ch, CURLOPT_POST, 1);
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        }
        if (is_array($headers) && count($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if ($method == Common::JSON_METHOD) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    public static function formatNumberToVPOS($value)
    {
        $number = number_format($value, 2, '.', '');
        return intval($number * 100);
    }

    public static function getCurrencyFromCode($code)
    {
        $moneda = Common::MONEDA_COLOMBIA;
        switch ($code) {
            case Common::CODIGO_MONEDA_COLOMBIA:
                $moneda = Common::MONEDA_COLOMBIA;
                break;

            case Common::CODIGO_MONEDA_DOLARES:
                $moneda = Common::MONEDA_DOLARES;
                break;

            default:
                $moneda = Common::MONEDA_COLOMBIA;
                break;
        }
        return $moneda;
    }

    public static function getGUIDnoHash()
    {
        mt_srand((double) microtime() * 10000);
        $charid = md5(uniqid(rand(), true));
        $c = unpack("C*", $charid);
        $c = implode("", $c);

        return substr($c, 0, 9);
    }

    public static function getIvaFromAmount($amount)
    {
        return number_format($amount * 0.19, 2, '.', '');
    }

    public static function getRandomAmount()
    {
        return number_format(10000000 / rand(1, 1000), 2, '.', '');
    }

    public static function registrarLog($method, $input, $output, $time, $pathLog)
    {
        $time = number_format(floatval($time), 5, '.', '');
        //error_log("[".date('c')."] [$method] [{$time} s] [INPUT: $input] [OUTPUT: $output]\n", 3, $pathLog);
    }

    public static function formatVPOSToKeosNumber($value)
    {
        $number = intval($value) / 100;
        return number_format($number, 2, '.', '');
    }

    public static function formatVPOSToNumber($value)
    {
        $number = intval($value) / 100;
        return number_format($number, 2, '.', ',');
    }

}
