<?php
date_default_timezone_set('America/Guayaquil');
$path = __DIR__;
require_once $path . "/lib/Common.php";
$token = isset($_REQUEST['token']) ? $_REQUEST['token'] : '';
$ipServer = $_SERVER['SERVER_ADDR'];
$env = $ipServer == Common::IP_PROD ? 'prod' : 'dev';
$pending = -1;
if (strlen($token) == 24) {

    $data = ["token" => $token];
    $headers = array(
        'Content-Type: application/json'
    );
    // Obtener los datos de la transacción del backend
    $url = str_replace("{token}", $token, $envParams[$env]['url_keos_gettransaction']);
    $beginTime = microtime(true);
    $getTransaction = Common::coreRequest($data, $url, "GET", $headers, Common::JSON_METHOD);
    $rTransaction = json_decode($getTransaction, true);
    $endTime = microtime(true);
    $totalTime = $endTime - $beginTime;
    $method = 'getTransaction';
    Common::registrarLog($method, json_encode($data), $getTransaction, $totalTime, $envParams[$env]['path_payment_log']);

    $msg_voucher = "";
    if ($rTransaction["code"] == 200) {
        $pending = 1;
        $amount = $rTransaction['data']['amount_due'];
        $iva = $rTransaction['data']['tax'];
        $ivaReturn = 0;
        $idTransaction = $rTransaction['data']['tn'];

        $payment_info = base64_encode(json_encode(["amount" => $amount, "iva" => $iva, "tx" => $idTransaction, "crypt_tk" => $token]));

        $msg_voucher = '<p>Se ha generado el siguiente link de pago:</p>';
        $msg_voucher .= '<div class="table-responsive">
            <table class="table text-center" style="font-size: 12px;">                                   
                <tbody>
                    <tr>
                        <th scope="row" class="borderNone">Código de transacción</th>
                        <td class="columnColor  borderNone">' . $idTransaction . '</td>
                    </tr>
                    <tr>
                        <th scope="row" class="borderNone">Monto a pagar</th>
                        <td class="columnColor  borderNone">' . $amount . ' COP</td>
                    </tr>                                                              
                </tbody>
                </table>
            </div>';
    } else {
        $pending = 0; //procesado
        $msg_voucher = $rTransaction["message"];
    }
} else {
    $msg_voucher = "No es posible procesar su solicitud.";
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Payrabbit - Checkout</title>
        <meta name="viewport" content="initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/payment.css?<?php echo rand(); ?>" >
    </head>
    <body>    
        <div class="navbar navbar-expand-md content align-items-center">
            <div class="img-fluid mr-4">
                <img src="images/logo_payrabbitmod.png" class="img-responsive" alt="">
            </div>
        </div>
    <center><div class="col-md-6 text-center">
            <div class="mt-3 card o-hidden">
                <div class="card-header card card-icon mb-2">
                </div>
                <div class="card-body">
                    <?php
                    if ($pending == 1) {
                        echo '<input type="hidden" id="pid" value="' . $payment_info . '"/>';
                    }
                    ?>
                    <h5 class="card-title text-uppercase font-weight-bold">Estado de la transacción</h5>
                    <p class="card-text"><?php echo $msg_voucher; ?></p>
                    <?php
                    if ($pending == 1) {
                        echo '<input type="button" id="btn_continue" value="Continuar" class="btn btn-primary btn-lg btn-block"/>';
                    }
                    ?>
                </div>
            </div>
        </div></center>

    <div class="footer fixed-bottom">
        <div class="mt-2 mr-5 float-right">
            <a target="_blank" href="https://www.facebook.com/payrabbit/"><img src="images/facebook.png" class="img-responsive"></a>
            <a target="_blank" href="https://www.instagram.com/payrabbit.co/"><img src="images/instagram.png" class="img-responsive" alt=""></a>
            <a target="_blank" href="https://www.linkedin.com/company/payrabbit/"><img src="images/linkedin.png" class="img-responsive" alt=""></a>
            <a target="_blank" href="https://wa.me/573004339567"><img src="images/whattsapp.png" class="img-responsive" alt=""></a>
            <a target="_blank" href="https://www.youtube.com/channel/UC8ir_D6i3LRVgOxEWHqmeNQ
               "><img src="images/youtube.png" class="img-responsive" alt=""></a>
        </div>
        <div class="mb-1 float-md-left pt-4 col-md-6 col-sm-12" style="z-index:-1">
            <p class="m-0 text-center">Copyright &copy;<?php echo date("Y"); ?> - Todos los Derechos Reservados</p>
        </div>
    </div>
    <script src="src/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="src/ajax/libs/sweetalert2/8.11.4/sweetalert2.all.min.js"></script>
    <?php if ($pending == 1) { ?>
        <script type="text/javascript">
            $(function () {
                $("#btn_continue").click(function (e) {
                    e.preventDefault();
                    $(this).attr("disabled", true);
                    $.ajax({
                        url: "ajax/checkout.php",
                        type: "POST",
                        dataType: "json",
                        data: {"pid": $("#pid").val()},
                        success: function (rsp) {
                            if (rsp.code === 200) {
                                window.location.href = rsp.url;
                            } else {
                                Swal.queue([{
                                        title: '<span class="text-info header-swal">AVISO</span>',
                                        type: 'warning',
                                        html: rsp.message
                                    }]);
                            }
                        },
                        error(xhr, status, error) {
                            console.log(error);
                        }
                    });
                });
            });
        </script>
        <?php
    }
    ?>
</body>
</html>