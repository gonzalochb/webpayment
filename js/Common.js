const PAYMENT_SELECT_TIPOS_DOCUMENTOS_EMPTYMESSAGE = 'Seleccione el tipo de documento...';
const PAYMENT_SELECT_GENEROS_EMPTYMESSAGE          = 'Seleccione el género...';
const PAYMENT_SELECT_DEPARTAMENTOS_EMPTYMESSAGE    = 'Seleccione el departamento...';
const PAYMENT_SELECT_CIUDADES_EMPTYMESSAGE         = 'Seleccione la ciudad...';
const COD_ERROR_SUCCESS          = 100;
const PAYMENT_METODO_PAGAR       = 'doPayment';
const AJAX_PAYMENT_INTERFACE     = '/ajax/ajax_payment.php';
const DEPARTMENTS_COLOMBIA_JSON  = '/lib/co_departments.json'
const CARGANDO_TEXT              = 'Cargando...';

var helpers =
{
    alert: function(message, type = null, title = null) {
        Swal.fire({
            title: title,
            html: message,
            type: type,
            confirmButtonText: 'Aceptar',
            buttonsStyling: false,
            customClass:{
                header: 'header-swal',
                confirmButton: 'btn btn-info',
                cancelButton: 'btn'
            }
        });
    },
    generateModalLoading: function(text) {
        $('body').loadingModal({
            position: 'auto',
            text: text,
            color: '#fff',
            opacity: '0.7',
            backgroundColor: '#0f265c',
            animation: 'doubleBounce'
        });
    },
    /**
    * sends a request to the specified url from a form. this will change the window location.
    * @param {string} path the path to send the post request to
    * @param {object} params the paramiters to add to the url
    * @param {string} [method=post] the method to use on the form
    */
    postToUrl: function(path, params, method='post') {
        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        const form = document.createElement('form');
        form.method = method;
        form.action = path;

        for (const key in params) {
          if (params.hasOwnProperty(key)) {
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = key;
            hiddenField.value = params[key];

            form.appendChild(hiddenField);
          }
        }

        document.body.appendChild(form);
        form.submit();
    }
}

