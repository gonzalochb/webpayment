$( document ).ready(function() {
    $('body').loading({
        stoppable: true,
        message: CARGANDO_TEXT
    });
    configInicial();

    $("#continuar").click(function() {
        var validacion = validarFormulario();
        if (validacion.error) {
            helpers.alert(validacion.msgError, 'error', `<span class="text-danger">Atención!</span>`)
        } else {
            continuar(validacion.params);
        }
    });
});

function configInicial() {
    sessionStorage.setItem("departamentos", null);
    getDepartamentos();
    generarComboCiudades(null);
    generarComboGenero();
    generarComboTipoDocumento();
    $('body').loading('stop');
}

function generarComboCiudades(data) {
    var rowPlaceholder  = {"id":"", "text":PAYMENT_SELECT_CIUDADES_EMPTYMESSAGE};
    var cmbCiudadesData = [rowPlaceholder];
    var dataDeps        = JSON.parse(sessionStorage.getItem("departamentos"));
    if (data) {
        var departamento    = $.grep(dataDeps, function( c ) {
            return c.iso == data.id;
        });
        $.each(departamento[0].ciudades, function(k, v) {
            var row = {"id":"", "text":""};
            row.id   = k;
            row.text = v;
            cmbCiudadesData.push(row);
        });
    }
    if ($('#ciudad').data('select2')) {
        $('#ciudad')
        .find('option')
        .remove()
        .end()
        .append('<option value="">'+PAYMENT_SELECT_CIUDADES_EMPTYMESSAGE+'</option>')
        .val('')
    }
    $("#ciudad").select2({
        data: cmbCiudadesData,
        placeholder: rowPlaceholder,
        allowClear: true,
        width: "100%",
        language: 'es'
    });
}

function generarComboDepartamentos(data) {
    var rowPlaceholder = {"id":"", "text":PAYMENT_SELECT_DEPARTAMENTOS_EMPTYMESSAGE};
    var cmbDepsData    = [rowPlaceholder];
    $.each(data, function(k, v) {
        var row = {"id":"", "text":""};
        row.id   = v.iso;
        row.text = v.departamento;
        cmbDepsData.push(row);
    });
    $("#departamento").select2({
        data: cmbDepsData,
        placeholder: rowPlaceholder,
        allowClear: true,
        width: "100%",
        language: 'es'
    });

    $("#departamento").on('select2:select', function (e) {
        var dataDepartamento = e.params.data;
        generarComboCiudades(dataDepartamento);
    });

    $('#departamento').on('select2:unselect', function (e) {
        $(this).val(null).trigger('change');
        $('#ciudad')
            .find('option')
            .remove()
            .end()
            .append('<option value="">'+PAYMENT_SELECT_CIUDADES_EMPTYMESSAGE+'</option>')
            .val('');
    });
}

function generarComboGenero() {
    var rowPlaceholder = {"id":"", "text":PAYMENT_SELECT_GENEROS_EMPTYMESSAGE};
    var cmbGenerosData = [
        {"id":"M", "text":"Masculino"},
        {"id":"F", "text":"Femenino"},
    ];
    cmbGenerosData.unshift(rowPlaceholder);
    $("#genero").select2({
        data: cmbGenerosData,
        placeholder: rowPlaceholder,
        allowClear: true,
        width: "100%",
        language: 'es'
    });
}

function generarComboTipoDocumento() {
    var rowPlaceholder = {"id":"", "text":PAYMENT_SELECT_TIPOS_DOCUMENTOS_EMPTYMESSAGE};
    var cmbTiposData = [
        {"id":"CC", "text":"Cédula de Ciudadanía"},
        {"id":"CE", "text":"Cédula de Extranjeria"},
        {"id":"NIT", "text":"Número de Identificación Tributaria"},
        {"id":"PA", "text":"Pasaporte"},
        {"id":"RUT", "text":"RUT"},
    ];
    cmbTiposData.unshift(rowPlaceholder);
    $("#tipo_documento").select2({
        data: cmbTiposData,
        placeholder: rowPlaceholder,
        allowClear: true,
        width: "100%",
        language: 'es'
    });
}

function getDepartamentos() {
    $.getJSON( DEPARTMENTS_COLOMBIA_JSON, function( data ) {
        sessionStorage.setItem("departamentos", JSON.stringify(data));
        generarComboDepartamentos(data);
    });
}

function continuar(params) {
    Swal.queue([{
        title: '<span class="text-info header-swal">Confirmación!</span>',
        type: 'info',
        html: 'Revise que la información sea correcta. Si está seguro(a) proceda con el pago.',
        showCancelButton: true,
        focusConfirm: false,
        cancelButtonText:
            'Cancelar',
        confirmButtonText:
            'Sí. Estoy seguro(a)',
        customClass: {
            cancelButton: 'btn btn-danger',
            confirmButton: 'ml-3 btn btn-success',
        },
        buttonsStyling: false,
        reverseButtons: true,
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading(),
        preConfirm: () => {
            helpers.postToUrl(AJAX_PAYMENT_INTERFACE, params);
        }
    }]);
}

function validarFormulario() {
    var numErrores       = 0;
    var msgError         = '';
    var validacion       = {error:true,numErrores:numErrores, msgError: msgError,params:''};
    var nombre = $.trim($('#nombre').val());
    if (!(/^[a-zA-ZáéíóúñÁÉÍÓÑ\s]{1,}$/.test(nombre))) {
        msgError += '<div class="w-100 text-justify text-default">* Debe ingresar un <strong>Nombre</strong> que contenga solo letras.</strong></div>';
        numErrores++;
    }
    var apellido = $.trim($('#apellido').val());
    if (!(/^[a-zA-ZáéíóúñÁÉÍÓÑ\s]{1,}$/.test(apellido))) {
        msgError += '<div class="w-100 text-justify text-default">* Debe ingresar un <strong>Apellido</strong> que contenga solo letras.</strong></div>';
        numErrores++;
    }
    /*var cmbGenero = $('#genero').select2('data');
    var genero    = cmbGenero[0].id;
    if (!genero.length) {
        msgError += '<div class="w-100 text-justify text-default">* Debe seleccionar un <strong>Género.</strong></div>';
        numErrores++;
    }*/
    var cmbDepartamento = $('#departamento').select2('data');
    var departamento    = cmbDepartamento[0].id;
    if (!departamento.length) {
        msgError += '<div class="w-100 text-justify text-default">* Debe seleccionar un <strong>Departamento.</strong></div>';
        numErrores++;
    }
    var cmbCiudad = $('#ciudad').select2('data');
    var ciudad    = cmbCiudad[0].text;
    if (ciudad == PAYMENT_SELECT_CIUDADES_EMPTYMESSAGE) {
        msgError += '<div class="w-100 text-justify text-default">* Debe seleccionar una <strong>Ciudad.</strong></div>';
        numErrores++;
    }
    var direccion = $.trim($('#direccion').val());
    if (!(/^[0-9a-zA-ZáéíóúñÁÉÍÓÑ.\-#\s]{1,}$/.test(direccion))) {
        msgError += '<div class="w-100 text-justify text-default">* Debe ingresar una <strong>Dirección</strong> y puede contener letras, números y los siguientes caracteres <span class="font-weight-bold"># - . </span></div>';
        numErrores++;
    }
    var postal = $.trim($('#postal').val());
    if (!(/^[0-9]{1,}$/.test(postal))) {
        msgError += '<div class="w-100 text-justify text-default">* Debe ingresar un <strong>Código Postal</strong> y debe contener sólo números.</strong></div>';
        numErrores++;
    }
    var telefono = $.trim($('#telefono').val());
    if (!(/^[0-9]{10,}$/.test(telefono))) {
        msgError += '<div class="w-100 text-justify text-default">* Debe ingresar un <strong>Teléfono</strong> y debe contener sólo números, mínimo 10 dígitos.</strong></div>';
        numErrores++;
    }
    var email = $.trim($('#email').val());
    if (!(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))) {
        msgError += '<div class="w-100 text-justify text-default">* Debe ingresar un <strong>Email</strong> válido.</strong></div>';
        numErrores++;
    }

    if (numErrores > 0) {
        validacion.numErrores = numErrores;
        validacion.msgError   = msgError;
        return validacion;
    }
    validacion.error   = false;
    var idacquirer     = $('#idacquirer').val();
    var idcommerce     = $('#idcommerce').val();
    var terminalcode   = $('#terminalcode').val();
    var amount         = $('#amount').val();
    var iva            = $('#iva').val();
    var ivaReturn      = $('#ivareturn').val();
    var idtrans        = $('#idtrans').val();
    var params = {
        amount: amount, idtrans: idtrans, nombre: nombre, apellido: apellido,
        /*genero: genero,*/ departamento: departamento, ciudad: encodeURIComponent(ciudad),
        direccion: encodeURIComponent(direccion), postal: postal, telefono: telefono, email: email,
        terminalcode: terminalcode, idacquirer: idacquirer, idcommerce:idcommerce,
        iva: iva, ivareturn: ivaReturn
    }
    validacion.params = params;
    return validacion;
}