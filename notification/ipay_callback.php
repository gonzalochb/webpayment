<?php

date_default_timezone_set('America/Guayaquil');
require_once "../lib/Common.php";

$ipServer = $_SERVER['SERVER_ADDR'];
$env = $ipServer == Common::IP_PROD ? 'prod' : 'dev';

#autoCallback al momento que el usuario presiona en pagar
if (isset($_REQUEST["mdOrder"], $_REQUEST["operation"])) {
    $orderId = $_REQUEST["mdOrder"];
    $operation = $_REQUEST["operation"];

    $headers = array('Content-Type: application/json');

    #Check order status
    $ipay_status_url = $envParams[$env]["ipay_api_order_status"];
    $tini = microtime(true);
    $ipay_status_data = ["orderId" => $orderId, "userName" => $envParams[$env]["ipay_api_usr"], "password" => $envParams[$env]["ipay_api_pwd"],];
    $rsp_orderStatus = json_decode(Common::coreRequest($ipay_status_data, $ipay_status_url, "POST", [], "HTTP"), true);
    $tlapse = round(microtime(true) - $tini, 4);

    $ticket_number = $rsp_orderStatus["orderNumber"];
    $rsp_code = (string) $rsp_orderStatus["actionCode"];

    unset($ipay_status_data["password"]);
    Common::registrarLog("getOrderStatusExtended", json_encode($ipay_status_data), json_encode($rsp_orderStatus), $tlapse, $envParams[$env]['path_payment_log']);

    #Send result to core
    $core_confirm_url = str_replace("{ticket_number}", $ticket_number, $envParams[$env]['core_set_psp_confirm']);
    $core_confirm_data = ["code" => $rsp_code, "weft" => $rsp_orderStatus];
    $tini = microtime(true);
    $rsp_confirm = Common::coreRequest($core_confirm_data, $core_confirm_url, "PUT", $headers, Common::JSON_METHOD);
    $tlapse = round(microtime(true) - $tini, 4);

    Common::registrarLog("ipay-confirm", json_encode($core_confirm_data), $rsp_confirm, $tlapse, $envParams[$env]['path_payment_log']);

    #DB log    
    $level = (isset($rsp_code)) ? (($rsp_code == "0") ? "INFO" : "WARNING") : "ERROR";
    $dlog = ['ticket_number' => $ticket_number, "level" => $level, "method" => "getOrderStatusExtended", "tlapse" => $tlapse,
        "req" => $ipay_status_data, "rsp_code" => $rsp_code, "rsp" => $rsp_orderStatus];
    Common::coreRequest($dlog, $envParams[$env]['url_keos_log'], 'POST', $headers, Common::JSON_METHOD);
}

#Callback manual
if (isset($_REQUEST["orderId"], $_REQUEST["action"])) {
    header("Location: https://payrabbit.co");
}


