<?php
date_default_timezone_set('America/Guayaquil');
/**
 * @filesource  /home/payrabbitdev/www/notification/response.php
 * @Descripcion Webhook que recibe la respuesta de Credibanco
 *
 *
 * PHP version 5
 *
 * @autor       Alejandro Barrionuevo R. <mbarrionuevo@anyway.com.ec>
 * @includes    1. /home/payrabbitdev/www/lib/Common.php
 *              2.
 * @fecha       2019/07/09
 * @flujo
 *
 * @Control de Cambio
 * @autor
 * @fecha
 * Descripcion
 */
require_once '/home/payrabbit/www/lib/Common.php';
require_once '/home/payrabbit/www/lib/XmlToArray.php';
require_once '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/beans/vpos_plugin.php';
$notification = file_get_contents('php://input');
$ipServer     = $_SERVER['SERVER_ADDR'];
$env          = $ipServer == Common::IP_DEV ? 'dev' : 'prod';
$response     = false;
parse_str($notification, $arrayIn);
//Vector de inicializacion
$VI = $envParams[$env]['vector_inicializacion'];
$estado   = Common::ESTADO_TRANSACCION_FALLIDA;
$arrayOut = array();

//llaves harcodeadas
$llaveVPOSSignPub    = file_get_contents($envParams[$env]['path_vpos_crb_sign_key']);
$llavePrivadaCifrado = file_get_contents($envParams[$env]['path_private_cifrado_key']);
$respuesta           = 'Transacción Rechazada';
$transExitosa  = false;
?>
<!doctype html>
<html>
    <head>
        <title><?php echo Common::GESTION_COMPROBANTE_TITLE ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>fontawesome/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>select2/4.0.7/css/select2.min.css" />
        <link rel="stylesheet" href="<?php echo Common::PATH_RECURSOS_GENERALES ?>jquery-loading/1.3.0/jquery.loading.min.css" />
        <link rel="stylesheet" href="/css/payment.css?<?php echo date("YmdHis") ?>" >
    </head>
    <body>
        <div class="row no-gutters shadow-sm mb-4 bg-white page-heading">
            <div class="col-12">
                <h2 class="text-center"><?php echo Common::GESTION_COMPROBANTE_TITLE ?></h2>
            </div>
        </div>
<?php
    if (is_array($arrayIn) && count($arrayIn)) {
        $beginTime = microtime(true);
        try {
            $response = Common::coreRequest(base64_encode(json_encode($arrayIn)), $envParams[$env]['url_vpos_response']);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        if (!$response) {
            $llavesesion  = BASE64URLRSA_decrypt($arrayIn['SESSIONKEY'],$llavePrivadaCifrado);
            $xmlDecifrado = BASE64URL_symmetric_decipher($arrayIn['XMLRES'],$llavesesion,$VI);
            $arrayOut = XmlToArray::convert(utf8_encode($xmlDecifrado));
            if (isset($arrayOut['VPOSTransaction1.2']) && is_array($arrayOut['VPOSTransaction1.2']) && count($arrayOut['VPOSTransaction1.2'])) {
                $arrayOut = $arrayOut['VPOSTransaction1.2'];
                $response = true;
            }
        }
        if($response) {
            if ($arrayOut['errorCode'] == Common::ERROR_CODE_SUCCESSFUL) {
                $estado       = Common::ESTADO_TRANSACCION_EXITOSA;
                $respuesta    = 'Transacción Aprobada';
                $transExitosa = true;
            }
        }
        $endTime   = microtime(true);
        $tlapse = round($endTime - $beginTime,5);
        $method    = 'VPOSResponse';
        unset($arrayIn['XMLRES']);
        Common::registrarLog($method, json_encode($arrayIn), json_encode($arrayOut), $tlapse, $envParams[$env]['path_payment_log']);

        $token = $arrayOut["purchaseOperationNumber"];
        $rsp_code = $arrayOut["errorCode"];
        $level = (isset($rsp_code)) ? (($rsp_code == "00") ? "INFO" : "WARNING") : "ERROR";
        $headers = array('Content-Type: application/json');
        $dlog = ['ticket_number' => $token, "level" => $level, "method" => "VPOSResponse", "tlapse" => $tlapse,
            "req" => $arrayIn, "rsp_code" => $rsp_code, "rsp" => $arrayOut];
        Common::coreRequest($dlog, $envParams[$env]['url_keos_log'], Common::POST_METHOD, $headers, Common::JSON_METHOD);

        if (count($arrayOut)) {
            $beginTime = microtime(true);
            $data = $arrayOut;
            $data['purchaseAmount'] = Common::formatVPOSToKeosNumber($arrayOut['purchaseAmount']);
            $data['purchaseIva']    = Common::formatVPOSToKeosNumber($arrayOut['purchaseIva']);
            $data['_ApplicationId'] = $envParams[$env]['keos_application_id'];
            $data['_JavaScriptKey'] = $envParams[$env]['keos_javascript_key'];
            $data['_ClientVersion'] = $envParams[$env]['keos_client_version'];
            $headers = array(
                'Content-Type: application/json'
            );

            $url = str_replace("{token}", $token, $envParams[$env]['url_keos_updateState']);
            $updateEstado = Common::coreRequest($data, $url, Common::POST_METHOD, $headers, Common::JSON_METHOD);
            $endTime      = microtime(true);
            $totalTime    = $endTime - $beginTime;
            $method       = 'setTransactionState';
            Common::registrarLog($method, json_encode($data), $updateEstado, $totalTime, $envParams[$env]['path_payment_log']);
?>
        <div class="row no-margin bg-white">
            <div class="col-10 col-sm-8 col-md-6 offset-1 offset-sm-2 offset-md-3">
                <div class="row mb-2">
                    <div class="col-12 text-center">VENTA NO PRESENCIAL</div>
                </div>
                <div class="row mb-2">
                    <div class="col-12 text-center"><?php echo Common::MENSAJE_CONFORMIDAD ?></div>
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="border-top <?php echo $transExitosa ? 'border-success' : 'border-danger' ?>">
                                Respuesta
                            </td>
                            <td class="border-top <?php echo $transExitosa ? 'border-success text-success' : 'border-danger text-danger' ?> text-right">
                                <?php echo $respuesta ?>
                            </td>
                        </tr>
<?php
                        if ($transExitosa) {
?>
                        <tr>
                            <td><strong>Número de Autorización</strong></td>
                            <td class="text-success text-right">
                                <?php echo $arrayOut['authorizationCode'] ?>
                            </td>
                        </tr>
<?php
                        } else {
?>
                        <tr>
                            <td>Código de Error</td>
                            <td class="text-danger text-right"><?php echo $arrayOut['errorCode'] ?></td>
                        </tr>
<?php
                        }
?>
                        <tr>
                            <td class="<?php echo $transExitosa ? 'border-top-0 border-bottom border-success' : '' ?>">Descripción de la Respuesta</td>
                            <td class="<?php echo $transExitosa ? 'border-top-0 border-bottom border-success text-success' : 'text-danger' ?> text-right">
                                <?php echo $arrayOut['errorMessage'] ?>
                            </td>
                        </tr>
<?php
                        if (!$transExitosa) {
?>
                        <tr>
                            <td class="border-top-0 border-bottom border-danger">
                                <strong>Resultado de la Autorización</strong>
                            </td>
                            <td class="border-top-0 border-bottom border-danger text-danger text-right">
                                <?php echo $arrayOut['authorizationResult'] ?>
                            </td>
                        </tr>
<?php
                        }
?>
                        <tr>
                            <td><strong>Nombre Comercio</strong></td>
                            <td class="text-right">CLIP CLAP</td>
                        </tr>
                        <tr>
                            <td>Código Único</td>
                            <td class="text-right"><?php echo $envParams[$env]['vpos_codigounico'] ?></td>
                        </tr>
                        <tr>
                            <td>Terminal</td>
                            <td class="text-right"><?php echo $arrayOut['purchaseTerminalCode'] ?></td>
                        </tr>
                        <tr>
                            <td>Número de Transacción</td>
                            <td class="text-right"><?php echo $arrayOut['purchaseOperationNumber'] ?></td>
                        </tr>
                        <tr>
                            <td>Fecha de la Transacción</td>
                            <td class="text-right"><?php echo date('d-m-Y'); ?></td>
                        </tr>
                        <tr>
                            <td>Hora de la Transacción</td>
                            <td class="text-right"><?php echo date('H:i:s') ?></td>
                        </tr>
                        <tr>
                            <td>Moneda</td>
                            <td class="text-right"><?php echo Common::getCurrencyFromCode($arrayOut['purchaseCurrencyCode']) ?></td>
                        </tr>
                        <tr>
                            <td>Valor Total</td>
                            <td class="text-right">$ <?php echo Common::formatVPOSToNumber($arrayOut['purchaseAmount']) ?></td>
                        </tr>
                        <tr>
                            <td>IVA</td>
                            <td class="text-right">$ <?php echo Common::formatVPOSToNumber($arrayOut['purchaseIva']) ?></td>
                        </tr>
                        <tr>
                            <td>Descripción</td>
                            <td class="text-right"><?php echo $arrayOut['additionalObservations'] ?></td>
                        </tr>
                        <tr>
                            <td>Referencia</td>
                            <td class="text-right"><?php echo $arrayOut['purchaseOperationNumber'] ?></td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group row">
                    <div class="border-0 form-control text-center">
                        <button type="button" class="btn btn-info" id="imprimir">
                            <i class="fas fa-print"></i> <?php echo Common::IMPRIMIR_TEXT ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
<?php
        } else {
?>
        <div class="row no-margin bg-white">
            <div class="col-10 col-sm-8 col-md-6 offset-1 offset-sm-2 offset-md-3">
                <div class="form-group row">
                    <div class="border-0 form-control">
                        <p class="text-danger text-center"><?php echo 'No se ha logrado desencriptar la información' ?></p>
                    </div>
                </div>
            </div>
        </div>
<?php
        }
    } else {
?>
    <div class="row no-margin bg-white">
        <div class="col-10 col-sm-8 col-md-6 offset-1 offset-sm-2 offset-md-3">
            <div class="form-group row">
                <div class="border-0 form-control">
                    <p class="text-danger text-center"><?php echo 'No existe información para mostrar' ?></p>
                </div>
            </div>
        </div>
    </div>
<?php
    }
?>
    </body>
    <!-- jquery -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>popper.js/1.14.7/umd/popper.min.js" ></script>
    <!-- bootstrap -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- sweetalert -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>sweetalert2/8.11.4/sweetalert2.all.min.js"></script>
    <!-- select2 -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>select2/4.0.7/js/select2.full.min.js"></script>
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>select2/4.0.7/i18n/es.js"></script>
    <!-- jquery-loading -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>jquery-loading/1.3.0/jquery.loading.min.js"></script>
    <!-- jQuery.print -->
    <script src="<?php echo Common::PATH_RECURSOS_GENERALES ?>jQuery.print/1.6.0/jQuery.print.min.js"></script>

    <!-- functional scripts -->
    <script src="/js/Common.js?<?php echo date("YmdHis") ?>"></script>
    <script src="/js/response.js?<?php echo date("YmdHis") ?>"></script>
</html>