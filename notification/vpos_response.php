<?php
date_default_timezone_set('America/Guayaquil');
/**
 * @filesource  /home/payrabbit/www/notification/vpos_response.php
 * @Descripcion desencripcion de la informacion
 *
 *
 * PHP version 5
 *
 * @autor       Alejandro Barrionuevo R. <mbarrionuevo@anyway.com.ec>
 * @includes    1. /home/payrabbit/www/lib/Common.php
 *              2. 
 * @fecha       2019/07/10
 * @flujo       
 * 
 * @Control de Cambio
 * @autor      
 * @fecha      
 * Descripcion 
 */

require_once '/home/payrabbit/www/lib/Common.php';
require_once '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/beans/vpos_plugin.php';

$data = isset($_REQUEST['data']) ? $_REQUEST['data'] : '';

$arrayIn  = json_decode(base64_decode($data),true);
$arrayOut = array();

$VI = $envParams[$env]['vector_inicializacion'];

$llaveVPOSSignPub    = file_get_contents($envParams[$env]['path_vpos_crb_sign_key']);
$llavePrivadaCifrado = file_get_contents($envParams[$env]['path_private_cifrado_key']);

$response = VPOSResponse($arrayIn,$arrayOut,$llaveVPOSSignPub,$llavePrivadaCifrado,$VI);

echo $arrayOut;