<?php

date_default_timezone_set('America/Guayaquil');
require_once "../lib/Common.php";

$ipServer = $_SERVER['SERVER_ADDR'];
$env = $ipServer == Common::IP_PROD ? 'prod' : 'dev';

$chk_rsp = ["code" => 401, "message" => "Unauthorized"];
if (isset($_REQUEST["pid"]) && !empty($_REQUEST["pid"])) {

    try {
        $pid = json_decode(base64_decode($_REQUEST["pid"]), true);
        $amount = $pid["amount"];
        $iva = $pid["iva"];
        $ticket_number = $pid["tx"];
        $token = $pid["crypt_tk"];

        #Order Registry
        $ipay_url = $envParams[$env]["ipay_api_register"];
        $ipay_data = ["amount" => Common::formatNumberToVPOS($amount), "currency" => 170, "language" => "es", "orderNumber" => $ticket_number,
            "jsonParams" => json_encode(["installments" => 1, "IVA.amount" => Common::formatNumberToVPOS($iva)]),
            "userName" => $envParams[$env]["ipay_api_usr"], "password" => $envParams[$env]["ipay_api_pwd"],
            "returnUrl" => $envParams[$env]["callback_url"] . "?action=success&tn={$ticket_number}",
            "failUrl" => $envParams[$env]["callback_url"] . "?action=failed&tn={$ticket_number}"];

        $tini = microtime(true);
        $orderCRB = json_decode(Common::coreRequest($ipay_data, $ipay_url, "POST", [], "HTTP"), true);
        
        /**
         * Array ( 
         * [orderId] => 5033d5a2-6cdf-7bff-b77b-2e6300b1eae4 
         * [formUrl] => https://ecouat.credibanco.com/payment/merchants/rbs/payment.html?mdOrder=5033d5a2-6cdf-7bff-b77b-2e6300b1eae4&language=es )
         */
        $tlapse = round(microtime(true) - $tini, 4);
        unset($ipay_data["password"]);

        Common::registrarLog("register", json_encode($ipay_data), json_encode($orderCRB), $tlapse, $envParams[$env]['path_payment_log']);

        if (isset($orderCRB["orderId"])) {
            $orderId = $orderCRB["orderId"];
            $urlRedirect = $orderCRB["formUrl"];

            //registrar metadata order (orderId, formUrl) en core
            /*
              $headers = array('Content-Type: application/json');
              $korder = ["ipay_order" => $orderCRB];
              $url_order = str_replace("{crypt_token}", $token, $envParams[$env]['core_set_psp_order']);
              $r_order = json_decode(Common::coreRequest($korder, $url_order, "PUT", $headers, Common::JSON_METHOD), true);
              $chk_rsp = ($r_order["code"] == 200) ? ["code" => 200, "message" => "OK", "url" => $urlRedirect] : $r_order;
             */
            $chk_rsp = ["code" => 200, "message" => "OK", "url" => $urlRedirect];
        } else {
            $headers = array('Content-Type: application/json');
            $rsp_code = $orderCRB["errorCode"];
            $core_confirm_url = str_replace("{ticket_number}", $ticket_number, $envParams[$env]['core_set_psp_confirm']);
            $core_confirm_data = ["code" => $rsp_code, "weft" => $orderCRB];

            $tini = microtime(true);
            $rsp_confirm = Common::coreRequest($core_confirm_data, $core_confirm_url, "PUT", $headers, Common::JSON_METHOD);
            $tlapse = round(microtime(true) - $tini, 4);

            Common::registrarLog("ipay-confirm", json_encode($core_confirm_data), $rsp_confirm, $tlapse, $envParams[$env]['path_payment_log']);

            #DB log
            $level = (isset($rsp_code)) ? (($rsp_code == "0") ? "INFO" : "WARNING") : "ERROR";
            $dlog = ['ticket_number' => $ticket_number, "level" => $level, "method" => "register", "tlapse" => $tlapse,
                "req" => $ipay_data, "rsp_code" => $rsp_code, "rsp" => $orderCRB];
            #Common::coreRequest($dlog, $envParams[$env]['url_keos_log'], 'POST', $headers, Common::JSON_METHOD);

            $chk_rsp = ["code" => 410, "message" => $orderCRB["errorMessage"]];
        }
    } catch (Exception $e) {
        $chk_rsp = ["code" => 410, "message" => "Datos incorrectos, no se puede procesar la solicitud."];
    }
}

echo json_encode($chk_rsp);
