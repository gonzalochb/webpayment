<?php
date_default_timezone_set('America/Guayaquil');
/**
 * @filesource  /home/payrabbit/www/ajax/ajax_payment.php
 * @Descripcion Proceso que valida la informacion para mostrar la caja de pago
 *
 *
 * PHP version 5
 *
 * @autor       Alejandro Barrionuevo R. <mbarrionuevo@anyway.com.ec>
 * @includes    1. /home/payrabbit/www/lib/Common.php
 *              2.
 * @fecha       2019/07/04
 * @flujo
 *
 * @Control de Cambio
 * @autor
 * @fecha
 * Descripcion
 */
require_once '/home/payrabbit/www/lib/Common.php';
require_once '/home/payrabbit/www/lib/vpos-plugin-php-2.0.3/beans/vpos_plugin.php';
$ipServer             = $_SERVER['SERVER_ADDR'];
$env                  = $ipServer == Common::IP_DEV ? 'dev' : 'prod';
$idAcquirer           = isset($_REQUEST['idacquirer'])       ? $_REQUEST['idacquirer']       : '';
$idCommerce           = isset($_REQUEST['idcommerce'])       ? $_REQUEST['idcommerce']       : '';
$terminalCode         = isset($_REQUEST['terminalcode'])     ? $_REQUEST['terminalcode']     : '';
$nombre               = isset($_REQUEST['nombre'])           ? $_REQUEST['nombre']           : '';
$apellido             = isset($_REQUEST['apellido'])         ? $_REQUEST['apellido']         : '';
//$genero               = isset($_REQUEST['genero'])           ? $_REQUEST['genero']           : '';
$departamento         = isset($_REQUEST['departamento'])     ? $_REQUEST['departamento']     : '';
$ciudad               = isset($_REQUEST['ciudad'])           ? $_REQUEST['ciudad']           : '';
$direccion            = isset($_REQUEST['direccion'])        ? $_REQUEST['direccion']        : '';
$postalCode           = isset($_REQUEST['postal'])           ? $_REQUEST['postal']           : '';
$telefono             = isset($_REQUEST['telefono'])         ? $_REQUEST['telefono']         : '';
$email                = isset($_REQUEST['email'])            ? $_REQUEST['email']            : '';
$idtrans              = isset($_REQUEST['idtrans'])          ? $_REQUEST['idtrans']          : '';
$amount               = isset($_REQUEST['amount'])           ? $_REQUEST['amount']           : '';
$iva                  = isset($_REQUEST['iva'])              ? $_REQUEST['iva']              : '';
$ivaReturn            = isset($_REQUEST['ivareturn'])        ? $_REQUEST['ivareturn']        : '';
$currencyCode         = Common::CODIGO_MONEDA_COLOMBIA;
$codigoPais           = Common::CODIGO_PAIS_ISO_COLOMBIA;
$language             = Common::CODIGO_LENGUAJE;
$purchasePlanId       = Common::VPOS_PLANID;
$purchaseQuotaId      = Common::VPOS_QUOTAID;
$nacionalidad         = Common::CODIGO_NACIONALIDAD;
$observacion          = Common::OBSERVACION_GENERICA . ' - ' . $idtrans;
$array_send['acquirerId']                  = $idAcquirer;
$array_send['commerceId']                  = $idCommerce;
$array_send['purchaseTerminalCode']        = $terminalCode;
$array_send['purchaseLanguage']            = $language;
$array_send['purchaseOperationNumber']     = $idtrans;
$array_send['purchaseAmount']              = Common::formatNumberToVPOS($amount);
$array_send['purchaseIva']                 = Common::formatNumberToVPOS($iva);
$array_send['purchaseIvaReturn']           = Common::formatNumberToVPOS($ivaReturn);
$array_send['purchaseCurrencyCode']        = $currencyCode;
$array_send['purchasePlanId']              = $purchasePlanId;
$array_send['purchaseQuotaId']             = $purchaseQuotaId;
$array_send['purchaseIpAddress']           = $ipServer;
$array_send['billingFirstName']            = $nombre;
$array_send['billingLastName']             = $apellido;
$array_send['billingCountry']              = $codigoPais;
$array_send['billingCity']                 = $ciudad;
$array_send['billingState']                = $departamento;
$array_send['billingAddress']              = $direccion;
$array_send['billingPhoneNumber']          = $telefono;
$array_send['billingCelPhoneNumber']       = $telefono;
$array_send['billingGender']               = '';//$genero;
$array_send['billingEmail']                = $email;
$array_send['billingNationality']          = $nacionalidad;
$array_send['additionalObservations']      = $observacion;
$array_send['fingerPrint']                 = $idtrans;
$array_send['billingPostalCode']           = $postalCode;
$array_send['shippingCountry']             = $codigoPais;
$array_send['shippingCity']                = $ciudad;
$array_send['shippingAddress']             = $direccion;
$array_send['shippingState']               = $departamento;
$array_send['shippingPostalCode']          = $postalCode;

//Setear un arreglo de cadenas con los parametros que seran devueltos
$array_get['XMLREQ']      = "";
$array_get['DIGITALSIGN'] = "";
$array_get['SESSIONKEY']  = "";

//Vector de inicializacion
$VI = $envParams[$env]['vector_inicializacion'];

//llaves harcodeadas
$llaveVPOSCryptoPub = file_get_contents($envParams[$env]['path_vpos_crb_crypto_key']);
$llavePrivFirma     = file_get_contents($envParams[$env]['path_private_firma_key']);
$beginTime          = microtime(true);
$rsend = VPOSSend($array_send, $array_get, $llaveVPOSCryptoPub, $llavePrivFirma, $VI);
$level = (is_bool($rsend)) ? (($rsend) ? "INFO" : "WARNING") : "ERROR";
$rsp_code = (is_bool($rsend)) ? (($rsend) ? "OK" : "FAIL") : "ERROR";
$method    = 'VPOSSend';
$endTime   = microtime(true);
$totalTime = round($endTime - $beginTime, 5);
$output    = array(
    'SESSIONKEY' => $array_get['SESSIONKEY'],
    'DIGITALSIGN' => $array_get['DIGITALSIGN']
);
Common::registrarLog($method, json_encode($array_send), json_encode($output), $totalTime, $envParams[$env]['path_payment_log']);

$headers = array('Content-Type: application/json');
$dlog = ['ticket_number' => $idtrans, "method" => $method, "level" => $level, "tlapse" => $totalTime, "req" => $array_send, "rsp_code" => $rsp_code, "rsp" => $output];
Common::coreRequest($dlog, $envParams[$env]['url_keos_log'], Common::POST_METHOD, $headers, Common::JSON_METHOD);
?>
<html>
    <body>
        <form id="form_payment" action="<?php echo $envParams[$env]['vpos_url']?>" method="POST">
            <INPUT TYPE="hidden" NAME="IDACQUIRER"  value="<?php echo $idAcquirer; ?>">
            <INPUT TYPE="hidden" NAME="IDCOMMERCE"  value="<?php echo $idCommerce; ?>">
            <INPUT TYPE="hidden" NAME="XMLREQ"      value="<?php echo $array_get['XMLREQ']; ?>">
            <INPUT TYPE="hidden" NAME="DIGITALSIGN" value="<?php echo $array_get['DIGITALSIGN']; ?>">
            <INPUT TYPE="hidden" NAME="SESSIONKEY"  value="<?php echo $array_get['SESSIONKEY']; ?>">
        </form>
        <script>
            document.getElementById("form_payment").submit();
        </script>
    </body>
</html>
